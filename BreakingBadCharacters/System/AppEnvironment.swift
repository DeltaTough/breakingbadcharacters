//
//  AppEnvironment.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 26/02/2021.
//

import UIKit
import Combine

struct AppEnvironment {
    let container: DIContainer
    let systemEventsHandler: SystemEventsHandler
}

extension AppEnvironment {
    
    static func bootstrap() -> AppEnvironment {
        let appState = Store<AppState>(AppState())
    
        let session = configuredURLSession()
        let webRepositories = configuredWebRepositories(session: session)
        let interactors = configuredInteractors(appState: appState, webRepositories: webRepositories)
        let systemEventsHandler = RealSystemEventsHandler(appState: appState)
        let diContainer = DIContainer(appState: appState, interactors: interactors)
        return AppEnvironment(container: diContainer,
                              systemEventsHandler: systemEventsHandler)
    }
    
    private static func configuredURLSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 120
        configuration.waitsForConnectivity = true
        configuration.httpMaximumConnectionsPerHost = 5
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        return URLSession(configuration: configuration)
    }
    
    private static func configuredWebRepositories(session: URLSession) -> WebRepositoriesContainer {
        let charactersWebRepository = RealCharactersWebRepository(
            session: session,
            baseURL: "https://breakingbadapi.com/api")
        let imageWebRepository = RealImageWebRepository(
            session: session,
            baseURL: "")
        return WebRepositoriesContainer(imageRepository: imageWebRepository,
                                        charactersRepository: charactersWebRepository)
    }
    
    private static func configuredInteractors(appState: Store<AppState>,
                                              webRepositories: WebRepositoriesContainer
    ) -> DIContainer.Interactors {
        let charactersInteractor = RealCharactersInteractor(
            webRepository: webRepositories.charactersRepository,
            appState: appState)
        let imagesInteractor = RealImagesInteractor(
            webRepository: webRepositories.imageRepository)
        return .init(charactersInteractor: charactersInteractor,
                     imagesInteractor: imagesInteractor)
    }
}

private extension AppEnvironment {
    struct WebRepositoriesContainer {
        let imageRepository: ImageWebRepository
        let charactersRepository: CharactersWebRepository
    }
}

