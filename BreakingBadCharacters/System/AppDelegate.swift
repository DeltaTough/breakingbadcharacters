//
//  AppDelegate.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 26/02/2021.
//

import UIKit
import Combine

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
