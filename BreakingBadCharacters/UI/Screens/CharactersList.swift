//
//  CharactersList.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import SwiftUI
import Combine

struct CharactersList: View {
    
    @Environment(\.injected) private var injected: DIContainer
    @State private var charactersSearch = CharactersSearch()
    @State private var routingState: Routing = .init()
    private var routingBinding: Binding<Routing> {
        $routingState.dispatched(to: injected.appState, \.routing.charactersList)
    }
    let inspection = Inspection<Self>()
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                self.content
                    .navigationBarTitle("Characters")
                    .navigationBarHidden(self.charactersSearch.keyboardHeight > 0)
                    .animation(.easeOut(duration: 0.3))
            }
            .modifier(NavigationViewStyle())
            .padding(.leading, self.leadingPadding(geometry))
        }
        .onReceive(keyboardHeightUpdate) { self.charactersSearch.keyboardHeight = $0 }
        .onReceive(charactersUpdate) { self.charactersSearch.all = $0 }
        .onReceive(routingUpdate) { self.routingState = $0 }
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
    
    private var content: AnyView {
        switch charactersSearch.filtered {
            case .notRequested: return AnyView(notRequestedView)
            case let .isLoading(last, _): return AnyView(loadingView(last))
            case let .loaded(countries): return AnyView(loadedView(countries, showSearch: true))
            case let .failed(error): return AnyView(failedView(error))
        }
    }
    
    private func leadingPadding(_ geometry: GeometryProxy) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            // A hack for correct display of the SplitView on iPads
            return geometry.size.width < geometry.size.height ? 0.5 : -0.5
        }
        return 0
    }
}

private extension CharactersList {
    struct NavigationViewStyle: ViewModifier {
        func body(content: Content) -> some View {
            #if targetEnvironment(macCatalyst)
            return content
            #else
            return content
                .navigationViewStyle(StackNavigationViewStyle())
            #endif
        }
    }
}

// MARK: - Side Effects

private extension CharactersList {
    func loadCharacters() {
        injected.interactors.charactersInteractor
            .loadCharacters()
    }
}

// MARK: - Loading Content

private extension CharactersList {
    var notRequestedView: some View {
        Text("").onAppear {
            self.loadCharacters()
        }
    }
    
    func loadingView(_ previouslyLoaded: [Character]?) -> some View {
        VStack {
            ActivityIndicatorView().padding()
            previouslyLoaded.map {
                loadedView($0, showSearch: false)
            }
        }
    }
    
    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            self.loadCharacters()
        })
    }
}

// MARK: - Displaying Content

private extension CharactersList {
    func loadedView(_ characters: [Character], showSearch: Bool) -> some View {
        VStack {
            if showSearch {
                SearchBar(text: $charactersSearch.searchText)
            }
            List(characters) { character in
                NavigationLink(
                    destination: self.detailsView(character: character),
                    tag: character.char_id,
                    selection: self.routingBinding.charactersDetails) {
                    CharacterCell(character: character)
                }
            }
        }.padding(.bottom, self.charactersSearch.keyboardHeight)
    }
    
    func detailsView(character: Character) -> some View {
        CharactersDetails(character: character)
    }
}

// MARK: - Filtering Countries

extension CharactersList {
    struct CharactersSearch {
        
        private(set) var filtered: Loadable<[Character]> = .notRequested
        var all: Loadable<[Character]> = .notRequested {
            didSet { filterCharacters() }
        }
        var searchText: String = "" {
            didSet { filterCharacters() }
        }
        var keyboardHeight: CGFloat = 0
        var locale = Locale.current
        
        private mutating func filterCharacters() {
            if searchText.count == 0 {
                filtered = all
            } else {
                filtered = all.map { characters in
                    characters.filter {
                        $0.name
                            .range(of: searchText, options: .caseInsensitive,
                                   range: nil, locale: nil) != nil
                    }
                }
            }
        }
    }
}

// MARK: - Routing

extension CharactersList {
    struct Routing: Equatable {
        var charactersDetails: Character.ID?
    }
}

// MARK: - State Updates

private extension CharactersList {
    
    var routingUpdate: AnyPublisher<Routing, Never> {
        injected.appState.updates(for: \.routing.charactersList)
    }
    
    var charactersUpdate: AnyPublisher<Loadable<[Character]>, Never> {
        injected.appState.updates(for: \.userData.characters)
    }
    
    var keyboardHeightUpdate: AnyPublisher<CGFloat, Never> {
        injected.appState.updates(for: \.system.keyboardHeight)
    }
}

#if DEBUG
struct CharactersList_Previews: PreviewProvider {
    static var previews: some View {
        CharactersList().inject(.preview)
    }
}
#endif

