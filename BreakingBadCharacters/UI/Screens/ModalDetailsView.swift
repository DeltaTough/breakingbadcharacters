//
//  ModalDetailsView.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 01/03/2021.
//

import SwiftUI

struct ModalDetailsView: View {
    
    let character: Character
    @Binding var isDisplayed: Bool
    let inspection = Inspection<Self>()
    
    var body: some View {
        NavigationView {
            VStack {
                character.img.map { url in
                    HStack {
                        Spacer()
                        ImageViewCharacter(imageURL: url)
                            .frame(width: 300, height: 200)
                        Spacer()
                    }
                }
                closeButton.padding(.top, 40)
            }
            .navigationBarTitle(Text(character.name), displayMode: .inline)
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
    
    private var closeButton: some View {
        Button(action: {
            self.isDisplayed = false
        }, label: { Text("Close") })
    }
}

#if DEBUG
struct ModalDetailsView_Previews: PreviewProvider {
    
    @State static var isDisplayed: Bool = true
    
    static var previews: some View {
        ModalDetailsView(character: Character.mockedData[0], isDisplayed: $isDisplayed)
            .inject(.preview)
    }
}
#endif

