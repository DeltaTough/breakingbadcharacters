//
//  CharactersDetails.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import SwiftUI
import Combine

struct CharactersDetails: View {
    
    let character: Character
    
    @Environment(\.injected) private var injected: DIContainer
    @State private var details: Loadable<Character.Details>
    @State private var routingState: Routing = .init()
    private var routingBinding: Binding<Routing> {
        $routingState.dispatched(to: injected.appState, \.routing.characterDetails)
    }
    let inspection = Inspection<Self>()
    
    init(character: Character, details: Loadable<Character.Details> = .notRequested) {
        self.character = character
        self._details = .init(initialValue: details)
    }
    
    var body: some View {
        content
            .navigationBarTitle(character.name)
            .modifier(NavigationBarBugFixer(goBack: self.goBack))
            .onReceive(routingUpdate) { self.routingState = $0 }
            .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
    
    private var content: AnyView {
        switch details {
            case .notRequested: return AnyView(notRequestedView)
            case .isLoading: return AnyView(loadingView)
            case let .loaded(characterDetails): return AnyView(loadedView(characterDetails))
            case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Side Effects

private extension CharactersDetails {
    func loadCharacterDetails() {
        injected.interactors.charactersInteractor
            .load(characterDetails: $details, character: character)
    }

    func showCharacterDetailsSheet() {
        injected.appState[\.routing.characterDetails.detailsSheet] = true
    }

    func goBack() {
        injected.appState[\.routing.charactersList.charactersDetails] = nil
    }
}

// MARK: - Loading Content

private extension CharactersDetails {
    var notRequestedView: some View {
        Text("").onAppear {
            self.loadCharacterDetails()
        }
    }

    var loadingView: some View {
        VStack {
            ActivityIndicatorView()
            Button(action: {
                self.details.cancelLoading()
            }, label: { Text("Cancel loading") })
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            self.loadCharacterDetails()
        })
    }
}

// MARK: - A workaround for a bug in NavigationBar
// https://stackoverflow.com/q/58404725/2923345

private struct NavigationBarBugFixer: ViewModifier {
    
    let goBack: () -> Void
    
    func body(content: Content) -> some View {
        #if targetEnvironment(simulator)
        let isiPhoneSimulator = UIDevice.current.userInterfaceIdiom == .phone
        return Group {
            if ProcessInfo.processInfo.isRunningTests {
                content
            } else {
                content
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading: Button(action: {
                        print("Please note that NavigationView currently does not work correctly on the iOS Simulator.")
                        self.goBack()
                    }, label: { Text(isiPhoneSimulator ? "Back" : "") }))
            }
        }
        #else
        return content
        #endif
    }
}

// MARK: - Displaying Content

private extension CharactersDetails {
    func loadedView(_ characterDetails: Character.Details) -> some View {
        List {
            character.img.map { url in
                characterImageView(url: url)
            }
            nameSectionSectionView(name: characterDetails.name)
            if characterDetails.occupation.count > 0 {
                occupationSectionSectionView(occupations: characterDetails.occupation)
            }
            statusSectionSectionView(status: characterDetails.status)
            nicknameSectionSectionView(nickname: characterDetails.nickname)
            if characterDetails.appearance.count > 0 {
                appearanceSectionView(appearances: characterDetails.appearance)
            }
        }
        .listStyle(GroupedListStyle())
        .sheet(isPresented: routingBinding.detailsSheet,
               content: { self.modalDetailsView() })
    }
    
    func characterImageView(url: URL) -> some View {
        HStack {
            Spacer()
            ImageViewCharacter(imageURL: url)
                .cornerRadius(10)
                .frame(width: 120, height: 80)
                .onTapGesture {
                    self.showCharacterDetailsSheet()
                }
            Spacer()
        }
    }
    
    func nameSectionSectionView(name: String) -> some View {
        Section(header: Text("Name")) {
            DetailRow(leftLabel: Text(name), rightLabel: "")
        }
    }
    
    func occupationSectionSectionView(occupations: [String]) -> some View {
        Section(header: Text("Occupation")) {
            ForEach(occupations, id: \.self) { occupation in
                DetailRow(leftLabel: Text(occupation), rightLabel: "Occupation")
            }
        }
    }
    func statusSectionSectionView(status: String) -> some View {
        Section(header: Text("Status")) {
            DetailRow(leftLabel: Text(status), rightLabel: "")
        }
    }
    func nicknameSectionSectionView(nickname: String) -> some View {
        Section(header: Text("Nickname")) {
            DetailRow(leftLabel: Text(nickname), rightLabel: "")
        }
    }
    
    func appearanceSectionView(appearances: [Int]) -> some View {
        Section(header: Text("Season appearance")) {
            ForEach(appearances, id: \.self) { appearance in
                DetailRow(leftLabel: Text("\(appearance)"), rightLabel: Text("Season"))
            }
        }
    }
    
    func modalDetailsView() -> some View {
        ModalDetailsView(character: character,
                         isDisplayed: routingBinding.detailsSheet)
            .inject(injected)
    }
}



// MARK: - Routing

extension CharactersDetails {
    struct Routing: Equatable {
        var detailsSheet: Bool = false
    }
}

// MARK: - State Updates

private extension CharactersDetails {
    
    var routingUpdate: AnyPublisher<Routing, Never> {
        injected.appState.updates(for: \.routing.characterDetails)
    }
}

// MARK: - Preview

#if DEBUG
struct CharactersDetails_Previews: PreviewProvider {
    static var previews: some View {
        CharactersDetails(character: Character.mockedData[0])
            .inject(.preview)
    }
}
#endif

