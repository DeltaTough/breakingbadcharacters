//
//  CharacterCell.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 26/02/2021.
//

import SwiftUI

struct CharacterCell: View {
    
    var character: Character
    
    var body: some View {
        HStack {
            character.img.map { url in
                characterImageView(url: url)
            }
        }
        Text(character.name)
            .lineLimit(0)
            .font(.title3)
            .padding()
            .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
    }
    
    func characterImageView(url: URL) -> some View {
        ImageViewCharacter(imageURL: url)
            .cornerRadius(10)
            .frame(width: 120, height: 80)
    }
}

#if DEBUG
struct CharacterCell_Previews: PreviewProvider {
    static var previews: some View {
        CharacterCell(character: Character.mockedData[0])
            .previewLayout(.fixed(width: 375, height: 60))
    }
}
#endif
