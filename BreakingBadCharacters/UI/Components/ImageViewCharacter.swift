//
//  ImageViewCharacter.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import SwiftUI
import Combine
import WebKit

struct ImageViewCharacter: View {
    
    let imageURL: URL
    @Environment(\.injected) var injected: DIContainer
    @State private var image: Loadable<UIImage>
    let inspection = Inspection<Self>()
    
    init(imageURL: URL, image: Loadable<UIImage> = .notRequested) {
        self.imageURL = imageURL
        self._image = .init(initialValue: image)
    }
    
    var body: some View {
        content
            .onReceive(inspection.notice) { self.inspection.visit(self, $0) }
    }
    
    private var content: AnyView {
        switch image {
            case .notRequested: return AnyView(notRequestedView)
            case .isLoading: return AnyView(loadingView)
            case let .loaded(image): return AnyView(loadedView(image))
            case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Side Effects

private extension ImageViewCharacter {
    func loadImage() {
        injected.interactors.imagesInteractor
            .load(image: $image, url: imageURL)
    }
}

// MARK: - Content

private extension ImageViewCharacter {
    var notRequestedView: some View {
        Text("").onAppear {
            self.loadImage()
        }
    }
    
    var loadingView: some View {
        ActivityIndicatorView()
    }
    
    func failedView(_ error: Error) -> some View {
        Text("Unable to load image")
            .font(.footnote)
            .multilineTextAlignment(.center)
            .padding()
    }
    
    func loadedView(_ image: UIImage) -> some View {
        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fit)
    }
}

#if DEBUG
struct ImageViewCharacter_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ImageViewCharacter(imageURL: URL(string: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg")!)
            ImageViewCharacter(imageURL: URL(string: "https://vignette.wikia.nocookie.net/breakingbad/images/9/95/JesseS5.jpg/revision/latest?cb=20120620012441")!)
            ImageViewCharacter(imageURL: URL(string: "https://s-i.huffpost.com/gen/1317262/images/o-ANNA-GUNN-facebook.jpg")!)
        }
    }
}
#endif

