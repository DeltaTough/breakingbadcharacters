//
//  Models.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import Foundation

struct Character: Codable, Equatable {
    let char_id: ID
    let name: String
    let img: URL?
    
    typealias ID = Int
}

extension Character {
    struct Details: Codable, Equatable {
        let img: URL?
        let name: String
        let occupation: [String]
        let status: String
        let nickname: String
        let appearance: [Int]
    }
}

// MARK: - Helpers

extension Character: Identifiable {
    var id: Int { char_id }
}

