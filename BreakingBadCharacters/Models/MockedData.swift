//
//  MockedData.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import Foundation

#if DEBUG

extension Character {
    static let mockedData: [Character] = [
        Character(char_id: 1, name: "Walter White", img: URL(string: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg")),
        Character(char_id: 2, name: "Jesse Pinkman", img: URL(string: "https://vignette.wikia.nocookie.net/breakingbad/images/9/95/JesseS5.jpg/revision/latest?cb=20120620012441")),
        Character(char_id: 3, name: "Skyler White", img: URL(string: "https://s-i.huffpost.com/gen/1317262/images/o-ANNA-GUNN-facebook.jpg"))
    ]
}

extension Character.Details {
    static var mockedData: [Character.Details] = {
        return [
            Character.Details(img: URL(string: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg"), name: "Walter White", occupation: ["High School Chemistry Teacher","Meth King Pin"
            ], status: "Presumed dead", nickname: "Heisenberg", appearance: [1,2,3,4,5]),
            Character.Details(img: URL(string: "https://vignette.wikia.nocookie.net/breakingbad/images/9/95/JesseS5.jpg/revision/latest?cb=20120620012441"), name: "Jesse Pinkman", occupation: ["House wife","Book Keeper","Car Wash Manager","Taxi Dispatcher"], status: "Alive", nickname: "Cap n' Cook", appearance: [1,2,3,4,5]),
            Character.Details(img: URL(string: "https://s-i.huffpost.com/gen/1317262/images/o-ANNA-GUNN-facebook.jpg"), name: "Skyler White", occupation: ["High School Chemistry Teacher","Meth King Pin"
            ], status: "Alive", nickname: "Sky", appearance: [1,2,3,4,5])
        ]
    }()
}

#endif

