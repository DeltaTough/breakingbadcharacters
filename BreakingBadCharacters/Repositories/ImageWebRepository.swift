//
//  ImageWebRepository.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import Combine
import UIKit

protocol ImageWebRepository: WebRepository {
    func load(imageURL: URL) -> AnyPublisher<UIImage, Error>
}

struct RealImageWebRepository: ImageWebRepository {
    let session: URLSession
    let baseURL: String
    let bgQueue = DispatchQueue(label: "bg_parse_queue")
    
    init(session: URLSession, baseURL: String) {
        self.session = session
        self.baseURL = baseURL
    }
    
    func load(imageURL: URL) -> AnyPublisher<UIImage, Error> {
        return download(rawImageURL: imageURL)
            .subscribe(on: bgQueue)
            .receive(on: DispatchQueue.main)
            .extractUnderlyingError()
            .eraseToAnyPublisher()
    }
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    private func download(rawImageURL: URL) -> AnyPublisher<UIImage, Error> {
        if let imageFromCache = imageCache.object(forKey: rawImageURL.absoluteString as NSString) {
            return Just(imageFromCache)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: URLRequest(url: rawImageURL))
            .tryMap { (data, response) in
                guard let image = UIImage(data: data)
                else { throw APIError.unexpectedResponse }
                imageCache.setObject(image, forKey: rawImageURL.absoluteString as NSString)
                return image
            }
            .eraseToAnyPublisher()
    }
}

