//
//  CharactersWebRepository.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import Combine
import Foundation

protocol CharactersWebRepository: WebRepository {
    func loadCharacters() -> AnyPublisher<[Character], Error>
    func loadCharactersDetails(character: Character) -> AnyPublisher<Character.Details, Error>
}

struct RealCharactersWebRepository: CharactersWebRepository {
    
    let session: URLSession
    let baseURL: String
    let bgQueue = DispatchQueue(label: "bg_parse_queue")
    
    init(session: URLSession, baseURL: String) {
        self.session = session
        self.baseURL = baseURL
    }
    
    func loadCharacters() -> AnyPublisher<[Character], Error> {
        return call(endpoint: API.allCharacters)
    }
    
    func loadCharactersDetails(character: Character) -> AnyPublisher<Character.Details, Error> {
        let request: AnyPublisher<[Character.Details], Error> = call(endpoint: API.characterDetails(character))
        return request
            .tryMap { array -> Character.Details in
                guard let details = array.first
                else { throw APIError.unexpectedResponse }
                return details
            }
            .eraseToAnyPublisher()
    }
}

// MARK: - Endpoints

extension RealCharactersWebRepository {
    enum API {
        case allCharacters
        case characterDetails(Character)
    }
}

extension RealCharactersWebRepository.API: APICall {
    var path: String {
        switch self {
            case .allCharacters:
                return "/characters"
            case let .characterDetails(character):
                let encodedName = character.name.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                return "/characters?name=\(encodedName ?? character.name)"
        }
    }
    var method: String {
        switch self {
            case .allCharacters, .characterDetails:
                return "GET"
        }
    }
    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }
    func body() throws -> Data? {
        return nil
    }
}

