//
//  AppState.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import SwiftUI
import Combine

struct AppState: Equatable {
    var userData = UserData()
    var routing = ViewRouting()
    var system = System()
}

extension AppState {
    struct UserData: Equatable {
        var characters: Loadable<[Character]> = .notRequested
    }
}

extension AppState {
    struct ViewRouting: Equatable {
        var charactersList = CharactersList.Routing()
        var characterDetails = CharactersDetails.Routing()
    }
}

extension AppState {
    struct System: Equatable {
        var isActive: Bool = false
        var keyboardHeight: CGFloat = 0
    }
}

func == (lhs: AppState, rhs: AppState) -> Bool {
    return lhs.userData == rhs.userData &&
        lhs.routing == rhs.routing &&
        lhs.system == rhs.system
}

#if DEBUG
extension AppState {
    static var preview: AppState {
        var state = AppState()
        state.userData.characters = .loaded(Character.mockedData)
        state.system.isActive = true
        return state
    }
}
#endif
