//
//  InteractorsContainer.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

extension DIContainer {
    struct Interactors {
        let charactersInteractor: CharactersInteractor
        let imagesInteractor: ImagesInteractor
        
        init(charactersInteractor: CharactersInteractor,
             imagesInteractor: ImagesInteractor) {
            self.charactersInteractor = charactersInteractor
            self.imagesInteractor = imagesInteractor
        }
        
        static var stub: Self {
            .init(charactersInteractor: StubCharactersInteractor(),
                  imagesInteractor: StubImagesInteractor())
        }
    }
}
