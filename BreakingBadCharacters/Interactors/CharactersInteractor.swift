//
//  CharactersInteractor.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 25/02/2021.
//

import Combine
import Foundation
import SwiftUI

protocol CharactersInteractor {
    func loadCharacters()
    func load(characterDetails: LoadableSubject<Character.Details>, character: Character)
}

struct RealCharactersInteractor: CharactersInteractor {
    
    let webRepository: CharactersWebRepository
    let appState: Store<AppState>
    
    init(webRepository: CharactersWebRepository, appState: Store<AppState>) {
        self.webRepository = webRepository
        self.appState = appState
    }
    
    func loadCharacters() {
        let cancelBag = CancelBag()
        appState[\.userData.characters].setIsLoading(cancelBag: cancelBag)
        weak var weakAppState = appState
        webRepository.loadCharacters()
            .sinkToLoadable { weakAppState?[\.userData.characters] = $0 }
            .store(in: cancelBag)
    }
    
    func load(characterDetails: LoadableSubject<Character.Details>, character: Character) {
        let cancelBag = CancelBag()
        characterDetails.wrappedValue.setIsLoading(cancelBag: cancelBag)
        let charactersArray = appState
            .map { $0.userData.characters }
            .tryMap { characters -> [Character] in
                if let error = characters.error {
                    throw error
                }
                return characters.value ?? []
            }
        webRepository.loadCharactersDetails(character: character)
            .combineLatest(charactersArray)
            .receive(on: webRepository.bgQueue)
            .map { $0.0 }
            .receive(on: DispatchQueue.main)
            .sinkToLoadable { characterDetails.wrappedValue = $0 }
            .store(in: cancelBag)
    }
}

struct StubCharactersInteractor: CharactersInteractor {
    
    func loadCharacters() {
    }
    
    func load(characterDetails: LoadableSubject<Character.Details>, character: Character) {
    }
}
