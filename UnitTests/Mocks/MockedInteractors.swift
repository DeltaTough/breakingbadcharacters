//
//  MockedInteractors.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import SwiftUI
import Combine
import ViewInspector
@testable import BreakingBadCharacters

extension DIContainer.Interactors {
    static func mocked(
        charactersInteractor: [MockedCharactersInteractor.Action] = [],
        imagesInteractor: [MockedImagesInteractor.Action] = []
    ) -> DIContainer.Interactors {
        .init(charactersInteractor: MockedCharactersInteractor(expected: charactersInteractor),
              imagesInteractor: MockedImagesInteractor(expected: imagesInteractor))
    }
    
    func verify(file: StaticString = #file, line: UInt = #line) {
        (charactersInteractor as? MockedCharactersInteractor)?
            .verify(file: file, line: line)
        (imagesInteractor as? MockedImagesInteractor)?
            .verify(file: file, line: line)
    }
}

// MARK: - CharactersInteractor

struct MockedCharactersInteractor: Mock, CharactersInteractor {
    
    enum Action: Equatable {
        case loadCharacters
        case loadCharactersDetails(Character)
    }
    
    let actions: MockActions<Action>
    
    init(expected: [Action]) {
        self.actions = .init(expected: expected)
    }
    
    func loadCharacters() {
        register(.loadCharacters)
    }
    
    func load(characterDetails: LoadableSubject<Character.Details>, character: Character) {
        register(.loadCharactersDetails(character))
    }
}

// MARK: - ImagesInteractor

struct MockedImagesInteractor: Mock, ImagesInteractor {
    
    enum Action: Equatable {
        case loadImage(URL?)
    }
    
    let actions: MockActions<Action>
    
    init(expected: [Action]) {
        self.actions = .init(expected: expected)
    }
    
    func load(image: LoadableSubject<UIImage>, url: URL?) {
        register(.loadImage(url))
    }
}

