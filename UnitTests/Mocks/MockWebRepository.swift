//
//  MockWebRepository.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import Combine
@testable import BreakingBadCharacters

class TestWebRepository: WebRepository {
    let session: URLSession = .mockedResponsesOnly
    let baseURL = "https://test.com"
    let bgQueue = DispatchQueue(label: "test")
}

// MARK: - CharactersWebRepository

final class MockedCharactersWebRepository: TestWebRepository, Mock, CharactersWebRepository {
    
    enum Action: Equatable {
        case loadCharacters
        case loadCharactersDetails(Character)
    }
    var actions = MockActions<Action>(expected: [])
    
    var charactersResponse: Result<[Character], Error> = .failure(MockError.valueNotSet)
    var detailsResponse: Result<Character.Details, Error> = .failure(MockError.valueNotSet)
    
    func loadCharacters() -> AnyPublisher<[Character], Error> {
        register(.loadCharacters)
        return charactersResponse.publish()
    }
    
    func loadCharactersDetails(character: Character) -> AnyPublisher<Character.Details, Error> {
        register(.loadCharactersDetails(character))
        return detailsResponse.publish()
    }
}

// MARK: - ImageWebRepository

final class MockedImageWebRepository: TestWebRepository, Mock, ImageWebRepository {
    
    enum Action: Equatable {
        case loadImage(URL?)
    }
    var actions = MockActions<Action>(expected: [])
    
    var imageResponse: Result<UIImage, Error> = .failure(MockError.valueNotSet)
    
    func load(imageURL: URL) -> AnyPublisher<UIImage, Error> {
        register(.loadImage(imageURL))
        return imageResponse.publish()
    }
}

