//
//  ContentViewTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import ViewInspector
@testable import BreakingBadCharacters

extension ContentView: Inspectable { }

final class ContentViewTests: XCTestCase {
    
    func test_content_for_tests() throws {
        let sut = ContentView(container: .defaultValue, isRunningTests: true)
        XCTAssertNoThrow(try sut.inspect().group().text(0))
    }
    
    func test_content_for_build() throws {
        let sut = ContentView(container: .defaultValue, isRunningTests: false)
        XCTAssertNoThrow(try sut.inspect().group().view(CharactersList.self, 0))
    }
}

