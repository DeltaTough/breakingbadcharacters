//
//  ImageViewCharacterTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import SwiftUI
import ViewInspector
@testable import BreakingBadCharacters

extension ImageViewCharacter: Inspectable { }

final class ImageViewCharacterTests: XCTestCase {
    
    let url = URL(string: "https://test.com/test.jpg")!
    
    func test_imageView_notRequested() {
        let interactors = DIContainer.Interactors.mocked(
            imagesInteractor: [.loadImage(url)])
        let sut = ImageViewCharacter(imageURL: url, image: .notRequested)
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.anyView().text())
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_imageView_isLoading_initial() {
        let interactors = DIContainer.Interactors.mocked()
        let sut = ImageViewCharacter(imageURL: url, image:
                                .isLoading(last: nil, cancelBag: CancelBag()))
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.anyView().view(ActivityIndicatorView.self))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_imageView_isLoading_refresh() {
        let interactors = DIContainer.Interactors.mocked()
        let image = UIColor.red.image(CGSize(width: 10, height: 10))
        let sut = ImageViewCharacter(imageURL: url, image:
                                .isLoading(last: image, cancelBag: CancelBag()))
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.anyView().view(ActivityIndicatorView.self))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_imageView_loaded() {
        let interactors = DIContainer.Interactors.mocked()
        let image = UIColor.red.image(CGSize(width: 10, height: 10))
        let sut = ImageViewCharacter(imageURL: url, image: .loaded(image))
        let exp = sut.inspection.inspect { view in
            let loadedImage = try view.anyView().image().actualImage().uiImage()
            XCTAssertEqual(loadedImage, image)
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 3)
    }
    
    func test_imageView_failed() {
        let interactors = DIContainer.Interactors.mocked()
        let sut = ImageViewCharacter(imageURL: url, image: .failed(NSError.test))
        let exp = sut.inspection.inspect { view in
            let message = try view.anyView().text().string()
            XCTAssertEqual(message, "Unable to load image")
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
}
