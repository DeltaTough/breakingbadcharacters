//
//  CharactersListTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import ViewInspector
@testable import BreakingBadCharacters

extension CharactersList: Inspectable { }
extension ActivityIndicatorView: Inspectable { }
extension CharacterCell: Inspectable { }
extension ErrorView: Inspectable { }

final class CharactersListTests: XCTestCase {
    
    func test_characters_notRequested() {
        let appState = AppState()
        XCTAssertEqual(appState.userData.characters, .notRequested)
        let interactors = DIContainer.Interactors.mocked(
            charactersInteractor: [.loadCharacters]
        )
        let sut = CharactersList()
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.content().text())
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(appState, interactors))
        wait(for: [exp], timeout: 2)
    }
}

final class CharactersListFilterTests: XCTestCase {
    
    func test_characters_filtering() {
        var sut = CharactersList.CharactersSearch()
        let characters = Character.mockedData
        sut.all = .loaded(characters)
        XCTAssertEqual(sut.filtered.value, characters)
        sut.searchText = characters[0].name
        XCTAssertEqual(sut.filtered.value, [characters[0]])
    }
}

// MARK: - CharactersList inspection helper

extension InspectableView where View == ViewType.View<CharactersList> {
    func content() throws -> InspectableView<ViewType.AnyView> {
        return try geometryReader().navigationView().anyView(0)
    }
    func firstRowLink() throws -> InspectableView<ViewType.NavigationLink> {
        return try content().vStack().list(1).forEach(0).hStack(0).navigationLink(0)
    }
}

