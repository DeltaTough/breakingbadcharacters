//
//  ModalDetailsViewTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import SwiftUI
import ViewInspector
@testable import BreakingBadCharacters

extension ModalDetailsView: Inspectable { }

final class ModalDetailsViewTests: XCTestCase {
    
    func test_modalDetails() {
        let character = Character.mockedData[0]
        let interactors = DIContainer.Interactors.mocked(
            imagesInteractor: [.loadImage(character.img)]
        )
        let isDisplayed = Binding(wrappedValue: true)
        let sut = ModalDetailsView(character: character, isDisplayed: isDisplayed)
        let exp = sut.inspection.inspect { view in
            let vStack = try view.navigationView().vStack(0)
            XCTAssertNoThrow(try vStack.hStack(0).view(ImageViewCharacter.self, 1))
            XCTAssertNoThrow(try vStack.button(1))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_modalDetails_close() {
        let character = Character.mockedData[0]
        let interactors = DIContainer.Interactors.mocked(
            imagesInteractor: [.loadImage(character.img)]
        )
        let isDisplayed = Binding(wrappedValue: true)
        let sut = ModalDetailsView(character: character, isDisplayed: isDisplayed)
        let exp = sut.inspection.inspect { view in
            XCTAssertTrue(isDisplayed.wrappedValue)
            try view.navigationView().vStack(0).button(1).tap()
            XCTAssertFalse(isDisplayed.wrappedValue)
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
}

