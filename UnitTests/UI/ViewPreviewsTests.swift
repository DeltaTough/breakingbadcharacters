//
//  ViewPreviewsTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import ViewInspector
@testable import BreakingBadCharacters

final class ViewPreviewsTests: XCTestCase {
    
    func test_contentView_previews() {
        _ = ContentView_Previews.previews
    }
    
    func test_countriesList_previews() {
        _ = CharactersList_Previews.previews
    }
    
    func test_countryDetails_previews() {
        _ = CharactersDetails_Previews.previews
    }
    
    func test_modalDetailsView_previews() {
        _ = ModalDetailsView_Previews.previews
    }
    
    func test_countryCell_previews() {
        _ = CharacterCell_Previews.previews
    }
    
    func test_detailRow_previews() {
        _ = DetailRow_Previews.previews
    }
    
    func test_errorView_previews() throws {
        let view = ErrorView_Previews.previews
        try view.inspect().view(ErrorView.self).actualView().retryAction()
    }
    
    func test_svgImageView_previews() {
        _ = ImageViewCharacter_Previews.previews
    }
}

