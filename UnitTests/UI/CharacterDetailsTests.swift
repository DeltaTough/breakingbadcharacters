//
//  CharacterDetailsTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import ViewInspector
@testable import BreakingBadCharacters

extension CharactersDetails: Inspectable { }
extension DetailRow: Inspectable { }

final class CharacterDetailsTests: XCTestCase {
    
    let character = Character.mockedData[0]
    
    func test_details_notRequested() {
        let interactors = DIContainer.Interactors.mocked(
            charactersInteractor: [.loadCharactersDetails(character)]
        )
        let sut = CharactersDetails(character: character, details: .notRequested)
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.content().text())
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_details_isLoading_initial() {
        let interactors = DIContainer.Interactors.mocked()
        let sut = CharactersDetails(character: character, details:
                                    .isLoading(last: nil, cancelBag: CancelBag()))
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.content().vStack().view(ActivityIndicatorView.self, 0))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_details_isLoading_refresh() {
        let interactors = DIContainer.Interactors.mocked()
        let sut = CharactersDetails(character: character, details:
                                    .isLoading(last: Character.Details.mockedData[0], cancelBag: CancelBag())
        )
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.content().vStack().view(ActivityIndicatorView.self, 0))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_details_loaded() {
        let interactors = DIContainer.Interactors.mocked(
            imagesInteractor: [.loadImage(character.img)]
        )
        let sut = CharactersDetails(character: character, details:
                                    .loaded(Character.Details.mockedData[0])
        )
        let exp = sut.inspection.inspect { view in
            let list = try view.content().list()
            XCTAssertNoThrow(try list.hStack(0).view(ImageViewCharacter.self, 1))
            let characterCode = try list.section(1).view(DetailRow.self, 0)
                .hStack().text(0).string()
            XCTAssertEqual(characterCode, self.character.name)
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 3)
    }
    
    func test_details_failed() {
        let interactors = DIContainer.Interactors.mocked()
        let sut = CharactersDetails(character: character, details: .failed(NSError.test))
        let exp = sut.inspection.inspect { view in
            XCTAssertNoThrow(try view.content().view(ErrorView.self))
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_details_failed_retry() {
        let interactors = DIContainer.Interactors.mocked(
            charactersInteractor: [.loadCharactersDetails(character)]
        )
        let sut = CharactersDetails(character: character, details: .failed(NSError.test))
        let exp = sut.inspection.inspect { view in
            let errorView = try view.content().view(ErrorView.self)
            try errorView.vStack().button(2).tap()
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(AppState(), interactors))
        wait(for: [exp], timeout: 2)
    }
    
    func test_sheetPresentation() {
        let interactors = DIContainer.Interactors.mocked(
            // Image is requested by CharactersDetails and Details sheet:
            imagesInteractor: [.loadImage(character.img),
                               .loadImage(character.img)]
        )
        let container = DIContainer(appState: .init(AppState()), interactors: interactors)
        XCTAssertFalse(container.appState.value.routing.characterDetails.detailsSheet)
        let sut = CharactersDetails(character: character, details: .loaded(Character.Details.mockedData[0]))
        let exp1 = sut.inspection.inspect { view in
            try view.content().list().hStack(0).view(ImageViewCharacter.self, 1).callOnTapGesture()
        }
        let exp2 = sut.inspection.inspect(after: 0.5) { view in
            XCTAssertTrue(container.appState.value.routing.characterDetails.detailsSheet)
            interactors.verify()
        }
        ViewHosting.host(view: sut.inject(container))
        wait(for: [exp1, exp2], timeout: 2)
    }
}

// MARK: - CharactersDetails inspection helper

extension InspectableView where View == ViewType.View<CharactersDetails> {
    func content() throws -> InspectableView<ViewType.AnyView> {
        return try anyView()
    }
}

