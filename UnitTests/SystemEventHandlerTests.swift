//
//  SystemEventHandlerTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import UIKit
@testable import BreakingBadCharacters

final class SystemEventsHandlerTests: XCTestCase {
    
    var sut: RealSystemEventsHandler!
    
    override func setUp() {
        sut = RealSystemEventsHandler(appState: .init(AppState()))
    }
    
    func test_didBecomeActive() {
        sut.sceneDidBecomeActive()
        var reference = AppState()
        XCTAssertFalse(reference.system.isActive)
        reference.system.isActive = true
        XCTAssertEqual(sut.appState.value, reference)
    }
    
    func test_willResignActive() {
        sut.sceneDidBecomeActive()
        sut.sceneWillResignActive()
        let reference = AppState()
        XCTAssertEqual(sut.appState.value, reference)
    }
    
    #if os(iOS) && !targetEnvironment(macCatalyst)
    func test_keyboardHeight() throws {
        let textFiled = UITextField(frame: .zero)
        let window = try XCTUnwrap(UIApplication.shared.windows.first, "Cannot extract the host view")
        window.makeKeyAndVisible()
        window.addSubview(textFiled)
        XCTAssertEqual(sut.appState.value.system.keyboardHeight, 0)
        textFiled.becomeFirstResponder()
        XCTAssertGreaterThan(sut.appState.value.system.keyboardHeight, 0)
        textFiled.removeFromSuperview()
    }
    #endif
}


