//
//  CharactersWebRepositoryTests.swift
//  BreakingBadCharacters
//
//  Created by Dimitrios Tsoumanis on 01/03/2021.
//

import XCTest
import Combine
@testable import BreakingBadCharacters

final class CharactersWebRepositoryTests: XCTestCase {
    
    private var sut: RealCharactersWebRepository!
    private var subscriptions = Set<AnyCancellable>()
    
    typealias API = RealCharactersWebRepository.API
    typealias Mock = RequestMocking.MockedResponse
    
    override func setUp() {
        subscriptions = Set<AnyCancellable>()
        sut = RealCharactersWebRepository(session: .mockedResponsesOnly,
                                          baseURL: "https://test.com")
    }
    
    override func tearDown() {
        RequestMocking.removeAllMocks()
    }
    
    // MARK: - All Characters
    
    func test_allCharacters() throws {
        let data = Character.mockedData
        try mock(.allCharacters, result: .success(data))
        let exp = XCTestExpectation(description: "Completion")
        sut.loadCharacters().sinkToResult { result in
            result.assertSuccess(value: data)
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_characterDetails() throws {
        let characters = Character.mockedData
        let value =
            Character.Details(img: URL(string: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg"), name: "Walter White", occupation: ["High School Chemistry Teacher","Meth King Pin"
            ], status: "Presumed dead", nickname: "Heisenberg", appearance: [1,2,3,4,5])
    
        try mock(.characterDetails(characters[0]), result: .success([value]))
        let exp = XCTestExpectation(description: "Completion")
        sut.loadCharactersDetails(character: characters[0]).sinkToResult { result in
            result.assertSuccess(value: value)
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_characterDetails_whenDetailsAreEmpty() throws {
        let characters = Character.mockedData
        try mock(.characterDetails(characters[0]), result: .success([Character.Details]()))
        let exp = XCTestExpectation(description: "Completion")
        sut.loadCharactersDetails(character: characters[0]).sinkToResult { result in
            result.assertFailure(APIError.unexpectedResponse.localizedDescription)
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    // MARK: - Helper
    
    private func mock<T>(_ apiCall: API, result: Result<T, Swift.Error>,
                         httpCode: HTTPCode = 200) throws where T: Encodable {
        let mock = try Mock(apiCall: apiCall, baseURL: sut.baseURL, result: result, httpCode: httpCode)
        RequestMocking.add(mock: mock)
    }
}

