//
//  ImageWebRepositoryTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import Combine
@testable import BreakingBadCharacters

final class ImageWebRepositoryTests: XCTestCase {
    
    private var sut: RealImageWebRepository!
    private var subscriptions = Set<AnyCancellable>()
    private lazy var testImage = UIColor.red.image(CGSize(width: 40, height: 40))
    
    typealias Mock = RequestMocking.MockedResponse
    
    override func setUp() {
        subscriptions = Set<AnyCancellable>()
        sut = RealImageWebRepository(session: .mockedResponsesOnly,
                                     baseURL: "https://test.com")
    }
    
    override func tearDown() {
        RequestMocking.removeAllMocks()
    }
    
    func test_loadImage_withoutConversion() throws {
        
        let imageURL = try XCTUnwrap(URL(string: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg"))
        let responseData = try XCTUnwrap(testImage.pngData())
        let mock = Mock(url: imageURL, result: .success(responseData))
        RequestMocking.add(mock: mock)
        
        let exp = XCTestExpectation(description: "Completion")
        sut.load(imageURL: imageURL).sinkToResult { result in
            switch result {
                case let .success(resultValue):
                    XCTAssertEqual(resultValue.size, self.testImage.size)
                case let .failure(error):
                    XCTFail("Unexpected error: \(error)")
            }
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
}
