//
//  CharactersInteractorTests.swift
//  UnitTests
//
//  Created by Dimitrios Tsoumanis on 02/03/2021.
//

import XCTest
import SwiftUI
import Combine
@testable import BreakingBadCharacters

final class CharactersInteractorTests: XCTestCase {
    
    let appState = CurrentValueSubject<AppState, Never>(AppState())
    var mockedRepository: MockedCharactersWebRepository!
    var sut: RealCharactersInteractor!
    var subscriptions = Set<AnyCancellable>()
    
    override func setUp() {
        appState.value = AppState()
        mockedRepository = MockedCharactersWebRepository()
        sut = RealCharactersInteractor(webRepository: mockedRepository, appState: appState)
        subscriptions = Set<AnyCancellable>()
    }
    
    // MARK: - loadCharacters
    
    func test_loadCharacters_notRequested_to_loaded() {
        let characters = Character.mockedData
        mockedRepository.charactersResponse = .success(characters)
        mockedRepository.actions = .init(expected: [
            .loadCharacters
        ])
        let updates = recordAppStateUserDataUpdates()
        sut.loadCharacters()
        let exp = XCTestExpectation(description: "Completion")
        updates.sink { updates in
            XCTAssertEqual(updates, [
                AppState.UserData(characters: .notRequested),
                AppState.UserData(characters: .isLoading(last: nil, cancelBag: CancelBag())),
                AppState.UserData(characters: .loaded(characters))
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_loadCharacters_loaded_to_loaded() {
        let initialCharacters = Character.mockedData
        let finalCharacters = [initialCharacters[0], initialCharacters[1]]
        appState[\.userData.characters] = .loaded(initialCharacters)
        mockedRepository.charactersResponse = .success(finalCharacters)
        mockedRepository.actions = .init(expected: [
            .loadCharacters
        ])
        let updates = recordAppStateUserDataUpdates()
        sut.loadCharacters()
        let exp = XCTestExpectation(description: "Completion")
        updates.sink { updates in
            XCTAssertEqual(updates, [
                AppState.UserData(characters: .loaded(initialCharacters)),
                AppState.UserData(characters: .isLoading(last: initialCharacters,
                                                        cancelBag: CancelBag())),
                AppState.UserData(characters: .loaded(finalCharacters))
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_loadCharacters_notRequested_to_failed() {
        let error = NSError.test
        mockedRepository.charactersResponse = .failure(error)
        mockedRepository.actions = .init(expected: [
            .loadCharacters
        ])
        let updates = recordAppStateUserDataUpdates()
        sut.loadCharacters()
        let exp = XCTestExpectation(description: "Completion")
        updates.sink { updates in
            XCTAssertEqual(updates, [
                AppState.UserData(characters: .notRequested),
                AppState.UserData(characters: .isLoading(last: nil, cancelBag: CancelBag())),
                AppState.UserData(characters: .failed(error))
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    // MARK: - loadCharactersDetails
    
    func test_loadCharactersDetails_characters_notRequested() {
        let character = Character.mockedData[0]
        let data = characterDetails(character: character
        )
        appState[\.userData.characters] = .notRequested
        mockedRepository.detailsResponse = .success(data)
        mockedRepository.actions = .init(expected: [
            .loadCharactersDetails(character)
        ])
        let details = BindingWithPublisher(value: Loadable<Character.Details>.notRequested)
        sut.load(characterDetails: details.binding, character: character)
        let exp = XCTestExpectation(description: "Completion")
        details.updatesRecorder.sink { updates in
            XCTAssertEqual(updates, [
                .notRequested,
                .isLoading(last: nil, cancelBag: CancelBag()),
                .loaded(data)
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_loadCharactersDetails_characters_loaded() {
        let characters = Character.mockedData
        let character = characters[0]
        
        let data = characterDetails(character: character)
        appState[\.userData.characters] = .loaded(characters)
        mockedRepository.detailsResponse = .success(data)
        mockedRepository.actions = .init(expected: [
            .loadCharactersDetails(character)
        ])
        let details = BindingWithPublisher(value: Loadable<Character.Details>.notRequested)
        sut.load(characterDetails: details.binding, character: character)
        let exp = XCTestExpectation(description: "Completion")
        details.updatesRecorder.sink { updates in
            XCTAssertEqual(updates, [
                .notRequested,
                .isLoading(last: nil, cancelBag: CancelBag()),
                .loaded(data)
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }
    
    func test_loadCharacterDetails_characters_failed() {
        let characters = Character.mockedData
        let character = characters[0]
        let error = NSError.test
        let data = characterDetails(character: character)
        appState[\.userData.characters] = .failed(error)
        mockedRepository.detailsResponse = .success(data)
        mockedRepository.actions = .init(expected: [
            .loadCharactersDetails(character)
        ])
        let details = BindingWithPublisher(value: Loadable<Character.Details>.notRequested)
        sut.load(characterDetails: details.binding, character: character)
        let exp = XCTestExpectation(description: "Completion")
        details.updatesRecorder.sink { updates in
            XCTAssertEqual(updates, [
                .notRequested,
                .isLoading(last: nil, cancelBag: CancelBag()),
                .failed(error)
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }

    func test_loadCharacterDetails_refresh() {
        let characters = Character.mockedData
        let character = characters[0]
        let data = characterDetails(character: character)
        appState[\.userData.characters] = .loaded(characters)
        mockedRepository.detailsResponse = .success(data)
        mockedRepository.actions = .init(expected: [
            .loadCharactersDetails(character)
        ])
        let details = BindingWithPublisher(value: Loadable<Character.Details>.loaded(data))
        sut.load(characterDetails: details.binding, character: character)
        let exp = XCTestExpectation(description: "Completion")
        details.updatesRecorder.sink { updates in
            XCTAssertEqual(updates, [
                .loaded(data),
                .isLoading(last: data, cancelBag: CancelBag()),
                .loaded(data)
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }

    func test_loadCharacterDetails_failure() {
        let error = NSError.test
        let characters = Character.mockedData
        let character = characters[0]
        appState[\.userData.characters] = .loaded(characters)
        mockedRepository.detailsResponse = .failure(error)
        mockedRepository.actions = .init(expected: [
            .loadCharactersDetails(character)
        ])
        let details = BindingWithPublisher(value: Loadable<Character.Details>.notRequested)
        sut.load(characterDetails: details.binding, character: character)
        let exp = XCTestExpectation(description: "Completion")
        details.updatesRecorder.sink { updates in
            XCTAssertEqual(updates, [
                .notRequested,
                .isLoading(last: nil, cancelBag: CancelBag()),
                .failed(error)
            ])
            self.mockedRepository.verify()
            exp.fulfill()
        }.store(in: &subscriptions)
        wait(for: [exp], timeout: 2)
    }

    func test_stubInteractor() {
        let sut = StubCharactersInteractor()
        sut.loadCharacters()
        let details = BindingWithPublisher(value: Loadable<Character.Details>.notRequested)
        sut.load(characterDetails: details.binding, character: Character.mockedData[0])
    }
    
    // MARK: - Helper
    
    private func recordAppStateUserDataUpdates(for timeInterval: TimeInterval = 0.5)
    -> AnyPublisher<[AppState.UserData], Never> {
        return Future<[AppState.UserData], Never> { (completion) in
            var updates = [AppState.UserData]()
            self.appState.map(\.userData)
                .sink { updates.append($0 )}
                .store(in: &self.subscriptions)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) {
                completion(.success(updates))
            }
        }.eraseToAnyPublisher()
    }
    
    private func characterDetails(character: Character)
    -> Character.Details {
        let details = Character.Details.mockedData[0]
        return (details)
    }
}
